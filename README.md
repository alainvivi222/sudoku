# sudoku

Ce code en C++ permet de générer des grilles de sudoku 9X9 et de vider aléatoirement le nombre de cases souhaitées (fichiers main.ccp et fonctions.h).

En exemple de sortie console, 'Capture grille 100 18 cases'.

NB : La suppression des cases étant aléatoire, rien ne dit que la grille dite 'jouable' le soit vraiment...

Dans le répertoire statistiques sudoku, un code en C++ permettant de faire des statistiques sur la génération de grilles.
