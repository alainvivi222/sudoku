#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

int tirage_numero();
bool verification_numero(int x, int y, int z);
int complete(int x, int y);
void reinitchiffreok();
bool verification_ligne(int x, int y, int z);
bool verification_colonne(int x, int y, int z);
bool verification_carre(int x, int y, int z);


#endif // FONCTIONS_H_INCLUDED

