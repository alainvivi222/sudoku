#include <iostream>
#include "fonctions.h"
using namespace std;


/* la supramatrice va stocker jusqu'à 100 grilles achevées [numéro supramatrice][colonne][ligne]
101 : pas de 0 donc 1 à 100
10  : case ligne supplémentaire pour stocker le nombre de matrices générées (le nombre de tentatives) pour arriver à une matrice sudoku complète correcte*/

int supramatrice[101][9][10];
int cpteur2 = 0;

/* la matrice va stocker la grille sudoku en cours de génération
[colonne][ligne]*/

int matrice [9][9] = {
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0} ,
    {0,0,0,0,0,0,0,0,0}
    };

/* variables globales :
c = colonne
l = ligne
chiffre = chiffre entre 1 et 9 généré aléatoirement
correct = booléen true = chiffre convient, false = ne convient pas
total = calcul des cumuls (ligne, colonne)
derligne = 45 (total chiffres de 1 à 9) - total = cumul des 8 premiers chiffres de la ligne ou de la colonne
n = compteur de passages consécutifs tirage aléatoire chiffre (car chiffre ne convient pas)
table mémoire chiffres = stockage des chiffres tirés aléatoirement consécutivement (car chiffre ne convient pas)
essai1 à essai9 = booléen true = chiffre final variable présent dans table mémoir chiffres, false = pas présent
tous = booléen true = essai1 à essai9 sur true, false = au moins 1 false
combien = nombre de grilles complètes et correctes à générer
cpteur = nombre de grilles générées
derdeder = booléen true = la dernière ligne est bien correcte, false = incorrecte
num = numéro de la supramatrice à rendre jouable
nbrecase = nombre de cases à conserver dans la supramatrice choisie*/

int c = 0;
int l = 0;
int chiffre= 0;
bool correct = true;
int total=0;
int derligne=0;
int n=0;
int memoirechiffres[100];
bool essai1=false;
bool essai2=false;
bool essai3=false;
bool essai4=false;
bool essai5=false;
bool essai6=false;
bool essai7=false;
bool essai8=false;
bool essai9=false;
bool tous = false;
int combien;
int cpteur = 1;
bool derdeder = true;
int num;
int nbrecase;


int main()
{
  cout << "Bienvenue dans ce generateur de grilles de sudoku 9 X 9 !" << endl;
  cout << "Indiquez le nombre de grilles a generer (maximum 100) : ";
  cin >> combien;
  cout << endl << endl;
  do{


    do{
            do{
                do{
                    tirage_numero();
                    chiffre = chiffre + 1;
                    verification_numero(chiffre,c,l);
                }while(correct==false);

                matrice [c][l] = chiffre;
                cout << matrice [c][l] << " ";
                reinitchiffreok();
                c = c + 1;
            }while(c<8);

		// Calcul du dernier de ligne par soustraction 45 - total des 8 premiers chiffres de la ligne

            derligne = complete(c,l);
            if(l==0){
                matrice[8][0] = derligne;
                cout << derligne;
                c=0;
                l++;
            }
            else if(l>0){
                int k=0;
                correct = true;
                do{
                    if(derligne == matrice[8][k]){
                        correct = false;
                    }
                    k++;
                }while((k<l)&& (correct == true));
                if(correct == true){
                    matrice [8][l] = derligne;
                    cout << derligne;
                    c=0;
                    l++;
                }
			// si le dernier chiffre calculé par soustraction (45 - total 8 premiers de la ligne) n'est pas possible (déjà présent colonne), tentative de retirer la même ligne

                else if (correct == false){
                        c=0;
                }
            }
            cout <<endl;
            reinitchiffreok();
    }while(l<8);

     // calcul de la dernière ligne par soustraction 45 - 8 premmier chiffres de la colonne

    for(int i=0;i<9;i++){
        matrice[i][8]=complete(i,l);
        cout << matrice [i][8] << " ";
    }
    cout<<endl <<endl;
    for(int t=0;t<9;t++){
        for(int u=0;u<9;u++){
            cout << matrice[u][t] << " ";
        }
        cout << endl;
    }

	// vérification dernière ligne correcte

    derdeder = true;
    total = 0;
    for(int i=0;i<9;i++){
        total = total + matrice[i][8];
    }
    if(total != 45){
        derdeder = false;
    }
    if(derdeder == true){
        int i=0;
        int j=0;
        do{
            j=i+1;
            do{
                if(matrice[i][8]==matrice[j][8]){
                derdeder=false;
                }
                j++;
            }while(j<9 && derdeder == true);
            i++;
        }while(i<8 && derdeder == true);
    }
    if(derdeder == true){
        cpteur2++;
		// stockage du numéro de la matrice généré dans extra case
	   supramatrice[cpteur2][0][9] = cpteur;

        for(int i=0;i<9;i++){
            for(int j=0;j<9;j++){
                supramatrice[cpteur2][j][i]=matrice[j][i];
                matrice[j][i]=0;
            }
        }
    }
    reinitchiffreok();
    c=0;
    l=0;
    cpteur++;
  }while(cpteur2 < combien);
  
  // affichage des supramatrices
  
  for(int z = 1;z <= cpteur2;z++){
    cout << endl << z << " (tentative " << supramatrice[z][0][9] << " )" << endl << endl;

    for(int i =0;i<9;i++){
        for(int j=0;j<9;j++){
            cout << supramatrice[z][j][i] << " ";
        }
        cout <<endl;
    }

  }
  do{
      cout << endl << "Indiquez quelle grille vous souhaitez rendre jouable : ";
      cin >> num;
  }while(num <1 || num>cpteur2);


  do{
      cout << endl << endl << "Indiquez combien de cases vous souhaitez conserver (minimum = 17) : ";
      cin >> nbrecase;
  }while(nbrecase<17 || nbrecase >80);

  // Supression aléatoire de cases (mise à 0)
  
  for(int i =0;i < 81-nbrecase;i++){
      if(i<2){
          c = tirage_numero();
          l = tirage_numero();
          supramatrice[num][c][l]=0;
      }
      else if(i>1){
          do{
            c = tirage_numero();
            l = tirage_numero();

          }while(supramatrice[num][c][l] == 0);
          supramatrice[num][c][l]=0;
      }
  }

  // affichage de la matrice jouable
  
  for(int i =0;i<9;i++){
        for(int j=0;j<9;j++){
            cout << supramatrice[num][j][i] << " ";
        }
        cout <<endl;
    }
}

// fonction permettant de tirer un numéro aléatoire entre 0 et 8

int tirage_numero(){
    /*uniform_int_distribution<>distr(0, 5);
    coul = distr(eng);*/
    int MAXC = 9;
    chiffre = rand() % MAXC;
    return(chiffre);
}

// fonction permettant de réinitialiser les variables et de vider le tableau memoirechiffres

void reinitchiffreok(){

    n=0;
    essai1 = false;
    essai2 = false;
    essai3 = false;
    essai4 = false;
    essai5 = false;
    essai6 = false;
    essai7 = false;
    essai8 = false;
    essai9 = false;

    for(int t=0;t<100;t++){
        memoirechiffres[t]=0;
    }
}

int complete(int x, int y){

    total = 0;
    if(y !=8 && x==8){
        for(int i = 0;i<8;i++){
            total = total + matrice [i][y];
        }
    }

    else if(y==8){
        for(int j= 0;j<8;j++){
            total = total + matrice [x][j];
        }
    }
    return(45-total);
}


/* fonction permettant de vérifier si le chiffre aléatoire tiré est possible.
Cette fonction fait appel à 1 à 3 des fonctions suivantes selon le positionnement dans la grilles : vérification_ligne, vérification colonne et vérification carré (3X3).*/

bool verification_numero(int x,int y,int z){
    correct = true;
    tous = false;
    int i=0;
    memoirechiffres[n]=chiffre;
    n++;

	// vérification que tous les chiffres de 1 à 9 ont déjà été tirés et donc, si c'est le cas, puisque aucun ne convient situation de blocage, et il faut passer à une nouvelle tentative de grille
    if(n>8){
        for(int m=0;m<n-2;m++){
            if(memoirechiffres[m]==1){
                    essai1=true;
            }
            if(memoirechiffres[m]==2){
                    essai2=true;
            }
            if(memoirechiffres[m]==3){
                    essai3=true;
            }
            if(memoirechiffres[m]==4){
                    essai4=true;
            }
            if(memoirechiffres[m]==5){
                    essai5=true;
            }
            if(memoirechiffres[m]==6){
                    essai6=true;
            }
            if(memoirechiffres[m]==7){
                    essai7=true;
            }
            if(memoirechiffres[m]==8){
                    essai8=true;
            }
            if(memoirechiffres[m]==9){
                    essai9=true;
            }
        }

        if((essai1 == true) && (essai2 == true) && (essai3 == true) && (essai4 == true) && (essai5 == true) && (essai6 == true) && (essai7 == true) && (essai8 == true) && (essai9 == true)){

                    cout<<endl <<endl;

                    int t=0;
                    int u=0;
                    do{
                        do{
                            cout << matrice[u][t] << " ";
                            matrice[u][t]=0;
                            u++;
                        }while((u<9) && ((u!=y) || (t!=z)));
                    cout << endl;
                    u=0;
                    t++;
                    }while(t<=z);

                    c=0;
                    l=0;
                    cpteur++;

                    cout << endl << endl << cpteur << endl << endl;

                    return(correct);
        }
        else{
            tous = false;
        }
    }

	/* si tous les chiffres n'ont pas encore été essayés, test du nouveau chiffre
	selon les cas (position dans le tableau) vérification_ligne et/ou vérification colonne et/ou vérification carré (3X3)
	pour rapple z = ligne, y = colonne, x = chiffre à tester*/

    if(n<9 || (n>8 && tous == false)){
        correct = true;
        if(z==0){
            if(y>0){
                    verification_ligne(x,y,z);
            }

        }
        else if ((z==1) || (z==2) || (z==4) || (z==5) || (z==7)){

            if(y>0){
                    verification_ligne(x,y,z);
            }
            if(correct == true){
                verification_colonne(x,y,z);
            }
            if(correct == true){
                verification_carre(x,y,z);
            }

        }

        else if (z==3 || z==6){
            if(y>0){
                    verification_ligne(x,y,z);
            }
            if(correct == true){
                verification_colonne(x,y,z);
            }

        }
        return(correct);
    }
    return(correct);

}

bool verification_ligne(int x, int y, int z){
    int i = 0;
    correct = true;
    do{
            if(x == matrice[i][z]){
                correct = false;
            }
            i++;
    }while((i < y) && (correct == true));
    return(correct);
}

bool verification_colonne(int x, int y, int z){
    int i = 0;
    correct = true;
    do{
        if(x == matrice[y][i]){
            correct = false;
        }
        i++;
    }while((i < z) && (correct == true));
    return(correct);
}

bool verification_carre(int x, int y, int z){
    int i = 0;
    correct = true;
    switch(z)
    {
    case 1:
        switch(y)
        {
        case 0:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);
            break;

        case 1:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 2:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 3:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 4:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 5:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 6:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;

        case 7:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;
        }
        break;

    case 2:
        switch(y)
        {
        case 0:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 1:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 2:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 3:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 4:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 5:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 6:
            i=6;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;

        case 7:
            i=6;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;
        }
        break;

    case 4:
        switch(y)
        {
        case 0:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 1:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 2:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 3:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 4:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 5:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 6:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;

        case 7:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;
        }

        break;

    case 5:
        switch(y)
        {
        case 0:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 1:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 2:
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 3:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 4:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 5:
            i=3;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 6:
            i=6;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;

        case 7:
            i=6;
            do{
                if((x == matrice[i][z-1]) || (x == matrice[i][z-2])){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;
        }
        break;

    case 7:
        switch(y)
        {
        case 0:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 1:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 2:
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<3 && correct == true);

            break;

        case 3:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 4:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 5:
            i=3;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<6 && correct == true);

            break;

        case 6:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;

        case 7:
            i=6;
            do{
                if(x == matrice[i][z-1]){
                    correct = false;
                }
                i++;
            }while(i<9 && correct == true);

            break;
        }

        break;

    }
    return(correct);

}
